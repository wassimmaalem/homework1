

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "person.h"

#define MALLOC(ptr,size) 
    do { 
        ptr=malloc(size); 
        if (!ptr) 
           abort(); 
    } while (0)

#define FREE(ptr) 
    do { 
        if (ptr) { 
            free(ptr); 
            ptr=NULL; 
        } 
    } while (0)

const int READ_LINE_SIZE = 1024;

#define READ_MODE "r"

static struct PersonRepository {
	int initialized;
	TAILQ_HEAD(p_list, Person) head;
} person_repository = {
	.initialized=0,
	 .head=NULL
};

 // initializing the  Person Repository
 
int person_init() {
	TAILQ_INIT(&person_repository.head);
	person_repository.initialized=1;
	return (EXIT_SUCCESS);
}

//clearing the person repository
 
int person_free() {
	if (!person_repository.initialized)
		return (EXIT_FAILURE);

	struct Person* p;
	while((p=person_remove_head())) {
		FREE(p->name);
		FREE(p->died);
		FREE(p);
	}
	return (EXIT_SUCCESS);
}


static struct Person* person_create(char *name, int age, enum PersonState state, char *died) {
	struct Person* result;
	if (!name || !*name) 
		return NULL;

	MALLOC(result, sizeof(struct Person));
	result->name = name;
	result->age = age;
	result->state = state;
	result->died = died;
	result->next.tqe_next = NULL;
	return (result);
}


struct Person *person_process_file_line(char *line) {
	if (!(line && *line))
		return NULL;

	struct Person *result=NULL;
	char *n = strtok(line, ";");
	char *a = strtok(NULL, ";");
	char *s = strtok(NULL, ";");
	char *d = strtok(NULL, ";");

#ifdef STRICT
	if (n && *n && a && *a && s && *s && d && *d)
		result = person_create(strdup(n),
	   						   atoi(a),
							   atoi(s),
							   strdup(d));
#else
	if (n && *n)
		result = person_create(strdup(n),
							   a ? atoi(a) : 0,
                               s ? atoi(s) : 0,
							   d ? strdup(d) : "?");
#endif
	return result;
}



static char *strChomp(char *buffer) {
	if (!buffer || !*buffer)
		return (buffer);
    
	char *begin = buffer;
    
	while (*buffer++)
		;
    
	while (--buffer >= begin) {
		if (*buffer >= ' ')
			return (begin);
    
		if (*buffer == '\r' || *buffer == '\n')
			*buffer = '\0';
	}
	return (begin);
}   


int person_load_from_file(char *fileName) {
	char *line;

	size_t len = READ_LINE_SIZE;
	struct Person *person;

	if (! (fileName && *fileName && person_repository.initialized))
		return (EXIT_FAILURE);

	FILE *inFile = fopen(fileName, READ_MODE);
	if (!inFile) {
		strerror(errno);
		return (EXIT_FAILURE);
	}

	MALLOC(line, READ_LINE_SIZE);

	while (fgets(line, len, inFile)) {
		person = person_process_file_line(strChomp(line));
		person_add(person);
	}

	FREE(line);
	fclose(inFile);
	return (EXIT_SUCCESS);
}


static char *person_get_state(const enum PersonState state) {
    switch (state) {
        case Married : return ("Married");
        case Divorced : return ("Divorced");
        case Single : return ("Single");
        case Widow : return ("Widow");
        default : return ("?");
    }
}


struct Person *person_find_by_name(char *name, struct Person* elm) {
    if (! (elm && name && *name && person_repository.initialized))
        return NULL;
    
    struct Person* iterator=elm;
    
    while((iterator=TAILQ_NEXT(iterator, next)))
    	if (strcmp(iterator->name, name)==0)
    		return (iterator);
   
    return NULL; 
}
   

struct Person *person_find_by_age(int age, struct Person* elm) {
    if (! (elm&&person_repository.initialized))
        return NULL;

    struct Person* iterator=elm;

    while((iterator=TAILQ_NEXT(iterator, next)))
    	if (iterator->age==age)
    		return (iterator);

    return NULL; 
}

int person_add(struct Person *p) {
	if (!(p&&person_repository.initialized))
		return (EXIT_FAILURE);

	TAILQ_INSERT_TAIL(&person_repository.head, p, next);
	return (EXIT_SUCCESS);
}

struct Person *person_remove_head() {
	if (!person_repository.initialized || TAILQ_EMPTY(&person_repository.head))
		return NULL;

	struct Person *p=TAILQ_FIRST(&person_repository.head);

	TAILQ_REMOVE(&person_repository.head, p, next);
	return (p);
}

void person_print_a_person(struct Person* p) {
	if (!p)
		return;
	printf("Name:%s, Age:%d, State:%s, Died at:%s\n", p->name, p->age, person_get_state(p->state), p->died ? p->died : "?");
}

void person_print_all() {
	if (!person_repository.initialized)
		return;

	struct Person* p;
	TAILQ_FOREACH(p, &person_repository.head, next)
		person_print_a_person(p);
}

struct Person *person_first() {
	return (person_repository.initialized
			? TAILQ_FIRST(&person_repository.head)
			: NULL);
}

struct Person *person_last() {
	return (person_repository.initialized
			? TAILQ_LAST(&person_repository.head, p_list)
			: NULL);
}

int person_compare_by_name(const struct Person *a, const struct Person *b) {
	return (a==b ? 0 :
			!a&&b ? -1 :
			!b&&a ? 1 :
			strcmp(a->name, b->name));
}

int person_compare_by_age(const struct Person *a, const struct Person *b) {
	return (a==b ? 0 :
			!a&&b ? -1 :
			!b&&a ? 1 :
			a->age-b->age);
}

int person_compare_by_state(const struct Person *a, const struct Person *b) {
	return (a==b ? 0 :
			!a&&b ? -1 :
			!b&&a ? 1 :
			a->state-b->state);
}

int person_compare_by_died(const struct Person *a, const struct Person *b) {
	return (a==b ? 0 :
			!a&&b ? -1 :
			!b&&a ? 1 :
			 !a->died&&b->died ?  -1 :
			 !b->died&&a->died ? 1 :
			 strcmp(a->died, b->died));
}


void person_bubble_sort(enum Ordering ordering, int (*comparer(const struct Person *, const struct Person *))) {
	if (!comparer)
		return;

	int change=0;
	do {
		change = 0;
		struct Person *p1=TAILQ_FIRST(&person_repository.head);
		struct Person *p2=TAILQ_NEXT(p1, next);
		while(p1&&p2) {
			int result = comparer(p1,p2);
			if (((ordering==ascending)&&(result>0))
				|| ((ordering==descending)&&(result<0))) {
				TAILQ_REMOVE(&person_repository.head, p1, next);
				TAILQ_INSERT_AFTER(&person_repository.head, p2, p1, next);
				change++;
				p2 = TAILQ_NEXT(p1, next);
			}
			else {
				p1=p2;
				p2=TAILQ_NEXT(p2, next);
			}
		}
	}while(change);
}
